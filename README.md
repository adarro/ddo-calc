# ddo-calc
## DDO Calculations, Planner and Plotting


DDO-Calc is a set of programs that I am creating to become more familiar with alternative JVM languages and multiple platforms.

I am also a fan of [DDO Online](http://www.ddo.com).

At the end, I would like to create a Character planner that includes the ability to discover / analyze equipment and support a database.  Target clients may include Android, Desktop via JavaFx and web (possible Google Web App).

The bulk of the codebase is targeted for Scala, with R providing some analytics / graphing of power curves etc.

The transport will eventually be an interconnected RESTful API using JSoN with optional Xml outputs utilizing Firebase as a potential backend.

![Travis CI](https://travis-ci.org/adarro/ddo-calc.svg?branch=master)
View on
- [OpenHub](https://www.openhub.net/p/ddo-calc)
- [Codecov](https://codecov.io/github/adarro/ddo-calc)


